# README #

# #
## Tower Research Python Web App Demo##

# #
To install the required packages, create a virtual environment and run:
    
```
#!bash

$ pip install -r requirements.txt
```

And then run the application:

```
#!Bash

$ python run.py
```

# #

To run the application tests:


```
#!Bash

$ cd tests
$ nosetests -sv
Test selecting data from movies database ... ok
Test creating a movie object and setting it's properties ... ok
Test inserting single movie to database ... ok

----------------------------------------------------------------------
Ran 3 tests in 0.138s

OK
```



### Application Structure ###

* The application has one module: "movies"
* It contains two models: a movie model, and a movies collection (of movie objects). Instead of using python's elegant WTForms to handle POST data (forms), I rely on my own custom modules and implement a basic validation function.
* The movie module has two views: A view to display the movies in the database, and a view to add a single new movie to the database
* A controller coordinates between the view and the model 
* A simple SQLite database stores the movie data locally
* The application uses Jinja2 template engine and twitter's Bootstrap for structuring the web interface



### Improvements ###
This is a very simple application to demonstrate python usage. Immediate improvements to the application would include:

* Proper graceful handling of database connection failure
* If a new movie submission fails validation, redirect the user to the new movie page and pre-populate the input fields + provide information on the cause of failure
* Implement models with WTForms