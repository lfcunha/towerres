# coding=utf-8
__author__ = 'luiscunha'

from flask import Flask
from flask import render_template
import sqlite3
from os import path
from contextlib import contextmanager


app = Flask(__name__)


# Configurations #
app.secret_key = 'my_super_duper_super_secret_key'
app.db = path.join(path.dirname(path.abspath(__file__)), "movies.db")
DATABASE = app.db
# To do: load these with app.config.from_object('config')
app.VALIDATION_FAIL = 0
app.INSERTION_FAIL = 2

@contextmanager
def db_cursor():
    """Use this as a context manager and it will automatically
    commit and close the database connection for you.
    Note that this is not in use elsewhere in the application because it requires multiple reuse of the generator
    Example:

        >>> with db_cursor() as db
        ...     db.execute("SELECT * FROM movies")
        ...     print db.fetchone()
    """
    if not path.exists(DATABASE):
        _create_database()

    conn = sqlite3.connect(DATABASE)
    yield conn.cursor()
    conn.commit()
    conn.close()

from app.movies.controller import mod_movies as movies_module
from app.movies.controller import mod_movie as movie_module

app.register_blueprint(movies_module)
app.register_blueprint(movie_module)


def _create_database():
    with db_cursor() as db:
        db.execute("CREATE TABLE movies (title TEXT, year INTEGER, genre VARCHAR(45), director VARCHAR(45), main_cast TEXT,"
              " mpaa_rating VARCHAR(30), my_rating INTEGER)")

    movie = ["Touching the Void", 2003, "Documentary", "Kevin Macdonald", "Simon Yates, Joe Simpson, Brendan Mackey",
             "R", 5]
    with db_cursor() as db:
        db.execute('INSERT INTO movies VALUES (?, ?, ?, ?, ?, ?, ?)', movie)

    movie = ["Memento", 2000, " Mystery, Thriller", "Christopher Noland", "Guy Pearce, Carrie-Anne Moss, Joe Pantoliano"
             , "R", 5]
    with db_cursor() as db:
        db.execute('INSERT INTO movies VALUES (?, ?, ?, ?, ?, ?, ?)', movie)

    movie = ["The Scent of Green Papaya", 1993, "Drama, Music, Romance", "Tran Anh Hung", "Tran Nu Yen-Khe, Man San Lu,"
             "Thi Loc Truong", "R", 4]
    with db_cursor() as db:
        db.execute('INSERT INTO movies VALUES (?, ?, ?, ?, ?, ?, ?)', movie)

    movie = ["Lust, Caution", 2007, "Drama, Romance, Thriller ", "Ang Lee", "Tony Chiu Wai Leung, Wei Tang, Joan Chen ",
             "R", 5]
    with db_cursor() as db:
        db.execute('INSERT INTO movies VALUES (?, ?, ?, ?, ?, ?, ?)', movie)



"""Create database, if it does not exist, an initiate with a few movie examples
"""
if not path.exists(DATABASE):
    _create_database()

if __name__ == "__main__":
    with db_cursor() as db:
        db.execute("select * from movies")
        print "movies1: ", db.fetchall()









