# coding=utf-8
__author__ = 'luiscunha'

import sqlite3
from datetime import date
import sys
from app import app

DATABASE = app.db


class Movies:
    """Class movies represents a collection of movies
    """

    def __init__(self, database=DATABASE):
        """ Inject database to allow for tests with custom database
        :param database:
        :return:
        """
        try:
            self._conn = sqlite3.connect(database)
            self._cursor = self._conn.cursor()
        except sqlite3.Error, e:
            # To do: log error and display a graceful error to the user
            pass
        self._movies_list = []

    def select(self):
        """Select all movies from database and return a list of movie objects
        :return _movies_list:
        """
        self._cursor.execute("SELECT * FROM movies")
        movies_list = self._cursor.fetchall()
        self._conn.close()
        for movie_ in movies_list:
            movie = Movie()
            movie.title, movie.year, movie.genre, movie.director, movie.main_cast, movie.mpaa_rating, movie.my_rating = \
                movie_[0], movie_[1], movie_[2], movie_[3], movie_[4], movie_[5], movie_[6]
            self._movies_list.append(movie)
        return self._movies_list


    @property
    def movies_list(self):
        return self.select()


class Movie:
    """Class Movie represents a single movie
    """

    def __init__(self, database=DATABASE):
        """Inject database to allow for tests with custom database
        :param database:
        :return:
        """
        self._conn = sqlite3.connect(database)
        self._cursor = self._conn.cursor()
        self._title = None
        self._year = None
        self._genre = None
        self._director = None
        self._main_cast = None
        self._mpaa_rating = None
        self._my_rating = None

    def unique(self):
        self._cursor.execute("select * from movies where title = ?", [self.title])
        self._conn.commit()
        return self._cursor.rowcount

    def validate(self):
        try:
            if len(self.title) == 0:
                return 0
            elif not self.unique():
                return 0
            elif len(self.year) == 0 or type(int(self.year)) != int or int(self.year) < 1900 or int(self.year) > \
                    date.today().year:
                return 0
            elif len(self.genre) == 0:
                return 0
            elif len(self.director) == 0:
                return 0
            elif len(self.main_cast) == 0:
                return 0
            elif len(self.mpaa_rating) == 0:
                return 0
            elif len(self.my_rating) == 0 or type(int(self.my_rating)) != int or int(self.my_rating) < 1 or \
                    int(self.my_rating) > 5:
                return 0
            else:
                return 1
        except ValueError:
            return 0

    def insert(self):
        if not self.validate():
            return app.VALIDATION_FAIL

        self._cursor.execute("INSERT INTO movies VALUES (?, ?, ?, ?, ?, ?, ?)", [self.title, self.year, self.genre,
                                                                                 self.director, self.main_cast,
                                                                                 self.mpaa_rating, self.my_rating])
        self._conn.commit()
        if self._cursor.rowcount == 1:
            return True
        else:
            return app.INSERTION_FAIL

    @property
    def title(self):
        return self._title

    @title.setter
    def title(self, title):
        self._title = title

    @property
    def year(self):
        return self._year

    @year.setter
    def year(self, year):
        self._year = year

    @property
    def genre(self):
        return self._genre

    @genre.setter
    def genre(self, genre):
        self._genre = genre

    @property
    def director(self):
        return self._director

    @director.setter
    def director(self, director):
        self._director = director

    @property
    def main_cast(self):
        return self._main_cast

    @main_cast.setter
    def main_cast(self, main_cast):
        self._main_cast = main_cast

    @property
    def mpaa_rating(self):
        return self._mpaa_rating

    @mpaa_rating.setter
    def mpaa_rating(self, mpaa_rating):
        self._mpaa_rating = mpaa_rating

    @property
    def my_rating(self):
        return self._my_rating

    @my_rating.setter
    def my_rating(self, my_rating):
        self._my_rating = my_rating


if __name__ == "__main__":
    m = Movies()
    for x in m.movies_list:
        print x.title
