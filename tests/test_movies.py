__author__ = 'luiscunha'


import sys
from os import path
sys.path.append(path.join(path.dirname(path.abspath(__file__)), "../"))
from app.movies.models import Movies
from app.movies.models import Movie
import sqlite3 as lite
from nose.tools import with_setup


DATABASE = "movies_test.db"


def _create_database():
    try:
        con = lite.connect(DATABASE)
        cur = con.cursor()
    except lite.Error, e:
        print "Error %s:" % e.args[0]
        sys.exit(1)
    cur.execute("CREATE TABLE IF NOT EXISTS movies (title TEXT, year INTEGER, genre VARCHAR(45), director VARCHAR(45),"
                " main_cast TEXT,mpaa_rating VARCHAR(30), my_rating INTEGER)")
    con.commit()
    movie = ["Touching the Void", 2003, "Documentary", "Kevin Macdonald", "Simon Yates, Joe Simpson, Brendan Mackey",
             "R", 5]
    cur.execute('INSERT INTO movies VALUES (?, ?, ?, ?, ?, ?, ?)', movie)
    con.commit()
    movie = ["Memento", 2000, " Mystery, Thriller", "Christopher Noland", "Guy Pearce, Carrie-Anne Moss, Joe Pantoliano"
             , "R", 5]
    cur.execute('INSERT INTO movies VALUES (?, ?, ?, ?, ?, ?, ?)', movie)
    con.commit()
    movie = ["The Scent of Green Papaya", 1993, "Drama, Music, Romance", "Tran Anh Hung", "Tran Nu Yen-Khe, Man San Lu,"
             "Thi Loc Truong", "R", 4]
    cur.execute('INSERT INTO movies VALUES (?, ?, ?, ?, ?, ?, ?)', movie)
    con.commit()
    movie = ["Lust, Caution", 2007, "Drama, Romance, Thriller ", "Ang Lee", "Tony Chiu Wai Leung, Wei Tang, Joan Chen ",
             "R", 5]
    cur.execute('INSERT INTO movies VALUES (?, ?, ?, ?, ?, ?, ?)', movie)
    con.commit()
    con.close()


def setup():
    """Create database if it does not exist, and populate with four records
    """
    if not path.exists(DATABASE):
        _create_database()


def teardown():
    pass


# Tests

@with_setup(setup, teardown)
def test_select():
    """Test selecting data from movies database
    """
    try:
        con = lite.connect(DATABASE)
        cur = con.cursor()
    except lite.Error, e:
        print "Error %s:" % e.args[0]
        sys.exit(1)
    # Truncate Table
    cur.execute("delete from movies")
    con.commit()
    movie = ["The Scent of Green Papaya", 1993, "Drama, Music, Romance", "Tran Anh Hung", "Tran Nu Yen-Khe, Man San Lu,"
             "Thi Loc Truong", "R", 4]
    cur.execute('INSERT INTO movies VALUES (?, ?, ?, ?, ?, ?, ?)', movie)
    con.commit()
    movie = ["Lust, Caution", 2007, "Drama, Romance, Thriller ", "Ang Lee", "Tony Chiu Wai Leung, Wei Tang, Joan Chen ",
             "R", 5]
    cur.execute('INSERT INTO movies VALUES (?, ?, ?, ?, ?, ?, ?)', movie)
    con.commit()
    con.close()
    m = Movies(DATABASE)
    movie_list = m.select()
    assert len(movie_list) > 0

def test_Movie():
    """Test creating a movie object and setting it's properties
    """
    movie = Movie()
    movie.title = "Super fun movie"
    movie .year = 2015
    movie.genre = "Comedy"
    movie.director = "John Smith"
    movie.main_cast = "Person1, Person2"
    movie.mpaa_rating = "R"
    movie.my_rating = 5
    assert movie.title == "Super fun movie" and movie.year == 2015 and movie.genre == "Comedy" and movie.director == \
        "John Smith" and movie.main_cast == "Person1, Person2" and movie.mpaa_rating == "R" and movie.my_rating == 5


def test_insert():
    """Test inserting single movie to database
    """
    # Truncate Table
    try:
        con = lite.connect(DATABASE)
        cur = con.cursor()
    except lite.Error, e:
        print "Error %s:" % e.args[0]
        sys.exit(1)
    # Truncate Table
    cur.execute("delete from movies")
    con.commit()

    # To do: test Movie's insertion method, instead of direct insertion
    movie = ["The Scent of Green Papaya", 1993, "Drama, Music, Romance", "Tran Anh Hung", "Tran Nu Yen-Khe, Man San Lu,"
             "Thi Loc Truong", "R", 4]
    cur.execute('INSERT INTO movies VALUES (?, ?, ?, ?, ?, ?, ?)', movie)
    con.commit()
    con.close()
    m = Movies(DATABASE)
    movie_list = m.select()
    assert len(movie_list) == 1